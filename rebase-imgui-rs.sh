#!/bin/sh

set -e

mkdir -p src
cp upstream/imgui-rs/imgui-sys/build.rs imgui.rs
cp upstream/imgui-rs/imgui-sys/include_imgui_docking.cpp .
rsync -Ert --inplace --no-whole-file --delete upstream/imgui-rs/imgui-sys/src/. src/imgui_sys
rsync -Ert --inplace --no-whole-file --delete upstream/imgui-rs/imgui/src/. src/imgui
rm -rf third-party
mkdir -p third-party
rsync -Ert --inplace --no-whole-file --delete upstream/imgui-rs/imgui-sys/third-party/imgui-docking third-party
rm -rf vendor/docs

find src/imgui_sys -name \*.rs \
     -exec sed -i \
     -e 's/crate::/crate::imgui_sys::/g' \
     -e 's/ imgui_sys::/ crate::imgui_sys::/g' \
     {} \+
find src/imgui -name \*.rs \
     -exec sed -i \
     -e 's/crate::/crate::imgui::/g' \
     -e 's%\(//[/!].*[& ]\)imgui::%\1ash_tray::imgui::%g' \
     -e 's/\([& ]\)imgui::/\1crate::imgui::/g' \
     -e 's/use crate::sys;//g' \
     -e 's/use crate::imgui::sys;//g' \
     -e 's/use sys::/use crate::imgui_sys::/g' \
     -e 's/\([ (&<[*]\)sys::/\1crate::imgui_sys::/g' \
     -e 's/\(\/\/[\/!].*[ (&<[*]\)im_str!/\1crate::im_str!/g' \
     -e 's/\([ (&<[*]\)im_str!/\1crate::im_str!/g' \
     -e 's/^pub extern crate /pub use crate::/' \
     {} \+

mv src/imgui_sys/lib.rs src/imgui_sys.rs
mv src/imgui/lib.rs src/imgui.rs

sed -i -e 's/^fn main/pub fn main/' imgui.rs

patch -p1 <<'EOF'
diff --git a/src/imgui_sys.rs b/src/imgui_sys.rs
index cf87954..c851d06 100644
--- a/src/imgui_sys.rs
+++ b/src/imgui_sys.rs
@@ -1,4 +1,3 @@
-#![no_std]
 // We use `chlorine` over (the more well known) `cty` right now since `cty`
 // doesn't fully match std::os::raw (leading to issues like
 // https://github.com/japaric/cty/issues/18). Chlorine *does* match std::os::raw
diff --git a/src/imgui.rs b/src/imgui.rs
index 9a7f64a..b9e3bb4 100644
--- a/src/imgui.rs
+++ b/src/imgui.rs
@@ -1,5 +1,4 @@
 #![cfg_attr(test, allow(clippy::float_cmp))]
-#![deny(rust_2018_idioms)]
 // #![deny(missing_docs)]
 
 pub use crate::imgui_sys as sys;
EOF

cargo fmt --all
cargo fmt --all
cargo clippy -- -D warnings -A unused-imports -A unknown-lints -A clippy::needless-borrow
cargo test --verbose
