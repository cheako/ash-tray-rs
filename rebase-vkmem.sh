#!/bin/sh

set -e

mkdir -p src
cp upstream/vk-mem-rs/build.rs vkmem.rs
rsync -Ert --inplace --no-whole-file --delete upstream/vk-mem-rs/src/. src/vk_mem
rsync -Ert --inplace --no-whole-file --delete upstream/vk-mem-rs/wrapper/. wrapper
rsync -Ert --inplace --no-whole-file --delete upstream/vk-mem-rs/vendor/. vendor
rm -f vendor/.git
rm -rf vendor/docs

perl -i -pe's/\R/\n/g' src/vk_mem/lib.rs vkmem.rs

find src/vk_mem -name \*.rs \
     -exec sed -i \
     -e 's/crate::/crate::vk_mem::/g' \
     -e 's/ vk_mem::/ ash_tray::vk_mem::/g' \
     -e 's%../gen/bindings.rs%../../gen/bindings.rs%g' \
     -e 's/(since = "0.3".*)//' \
     {} \+

mv src/vk_mem/lib.rs src/vk_mem.rs
echo ed src/vk_mem.rs <<'EOF'
/^extern crate/ d
d
d
d
d
i
use bitflags::bitflags;
.
w
EOF

set +e
patch -fp1<<'EOF'
--- a/src/vk_mem.rs
+++ b/src/vk_mem.rs
@@ -1431,6 +1431,7 @@ impl Allocator {
     /// If the flag was not used, the value of pointer `user_data` is just copied to
     /// allocation's user data. It is opaque, so you can use it however you want - e.g.
     /// as a pointer, ordinal number or some handle to you own data.
+    /// # Safety
     pub unsafe fn set_allocation_user_data(
         &self,
         allocation: &Allocation,
--- a/vkmem.rs
+++ b/vkmem.rs
@@ -1,10 +1,6 @@
-#[cfg(feature = "generate_bindings")]
-extern crate bindgen;
-extern crate cc;
-
 use std::env;
 
-fn main() {
+pub fn main() {
     let mut build = cc::Build::new();
 
     build.include("vendor/src");
@@ -38,7 +38,7 @@ pub fn main() {
     let source_files = ["wrapper/vma_lib.cpp"];
 
     for source_file in &source_files {
-        build.file(&source_file);
+        build.file(source_file);
     }
 
     let target = env::var("TARGET").unwrap();
@@ -88,6 +63,8 @@
             .flag("-Wno-reorder")
             .cpp_link_stdlib("stdc++")
             .cpp(true);
+    } else {
+        unimplemented!()
     }
 
     build.compile("vma_cpp");
--- b/src/imgui/drag_drop.rs
+++ a/src/imgui/drag_drop.rs
@@ -28,7 +28,7 @@
 //! For examples of each payload type, see [DragDropSource].
 use std::{any, ffi, marker::PhantomData};
 
+use crate::imgui::{Condition, Ui};
-use crate::imgui::{sys, Condition, Ui};
 use bitflags::bitflags;
 
 bitflags!(
diff --git b/src/imgui/window/child_window.rs a/src/imgui/window/child_window.rs
index 160070b..f5dd44a 100644
--- b/src/imgui/window/child_window.rs
+++ a/src/imgui/window/child_window.rs
@@ -2,8 +2,8 @@ use std::f32;
 
 use crate::imgui::math::MintVec2;
 use crate::imgui::window::WindowFlags;
+use crate::imgui::Id;
 use crate::imgui::Ui;
-use crate::imgui::{sys, Id};
 
 /// Builder for a child window
 #[derive(Copy, Clone, Debug)]
EOF
set -e
find -name \*.orig -delete
find -name \*.rej -delete

cargo fmt --all
cargo fmt --all
cargo clippy -- -D warnings -A unknown-lints -A clippy::needless-borrow
cargo test --verbose