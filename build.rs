mod imgui;
mod vkmem;

fn main() -> std::io::Result<()> {
    let r = imgui::main();
    vkmem::main();
    r
}
